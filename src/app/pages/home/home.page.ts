import { NavController } from "@ionic/angular";
import { Component, OnInit } from "@angular/core";
import { UtilitiesService } from "src/app/services/utilities/utilities.service";
@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit {
  persona:any;
  token:any;


  constructor(
    private navCtrl: NavController,
    private utilities: UtilitiesService,
  ) {
    // get data
     this.persona = JSON.parse(localStorage.getItem('user'))
     console.log(this.persona)

    // get token
    this.token = JSON.parse(localStorage.getItem('token'))
    console.log(this.token)
    this.token = JSON.parse(this.token)
  }

  ngOnInit() {
    this.utilities.welcomAlert('¡Bienvenido a Coink!' , '¡Cuenta creada exitosamente, tu marrano ya está listo para que empieces a ahorrar!')
  }

  goTo(url) {
    this.navCtrl.navigateForward(url);
  }
}
