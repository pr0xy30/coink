import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.page.html',
  styleUrls: ['./terms-conditions.page.scss'],
})
export class TermsConditionsPage implements OnInit {
  option:any = "A";
  check:boolean= true;

  constructor(    private navCtrl: NavController,
    ) { }

  ngOnInit() {
  }

  goTo(url) {
    if(this.check){
       this.navCtrl.navigateForward(url);
    }
  }


}
