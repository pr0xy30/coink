import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  user: any;
  constructor(private navCtrl: NavController) {
        // get data
        this.user = JSON.parse(localStorage.getItem('user'))
        console.log(this.user)
  }

  async ngOnInit() {
  }

  close() {
    localStorage.clear();
    this.navCtrl.navigateRoot("/");
  }

  goTo(url) {
    this.navCtrl.navigateForward(url);
  }
}
