import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { CoinkService } from 'src/app/services/coink/coink.service';
import { UtilitiesService } from 'src/app/services/utilities/utilities.service';

@Component({
  selector: 'app-register-step-two',
  templateUrl: './register-step-two.page.html',
  styleUrls: ['./register-step-two.page.scss'],
})
export class RegisterStepTwoPage implements OnInit {
  option:any = "A";
  formGroup: FormGroup;
  gender:any
  document:any
  inputType ='password';
  inputType2 ='password';



  constructor(private fb: FormBuilder,    
              private coinkservice: CoinkService,
              private utilities: UtilitiesService,
              private navCtrl: NavController,
    ) {
    this.formGroup = this.fb.group({
      number_document: [
        "",
        Validators.compose([Validators.required,]),
      ],
      date_issue: [
        "",
        Validators.compose([Validators.required,]),
      ],
      gender: [
        "",
        Validators.compose([Validators.required,]),
      ],
      type_document: [
        "",
        Validators.compose([Validators.required,]),
      ],
      date_birth:[
        "",
        Validators.compose([Validators.required,]),
      ],
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
        ]),
      ],
      confirm_email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
          this.EmailValidator('email'),
        ]),
      ],
      password: [
        "",
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
      confirm_password: [
        "",
        Validators.compose(
          [Validators.required,
            Validators.minLength(6),
            this.PasswordValidator('password')
          ]),
      ],
      
    })


  }

  ngOnInit() {
    this.getData()
  }

  sendForm(){
    if(this.formGroup.status != 'INVALID'){
      // ENVIAR AL INDEX
      console.log("VALIDO")
      localStorage.setItem("user", JSON.stringify(this.formGroup.value));
      this.goTo('/terms-conditions')
    }
  }



  PasswordValidator(confirmPasswordInput: string) {
    let confirmPasswordControl: FormControl;
    let passwordControl: FormControl;
    
    return (control: FormControl) => {
      if (!control.parent) {
        return null;
      }
    
      if (!confirmPasswordControl) {
        confirmPasswordControl = control;
        passwordControl = control.parent.get(confirmPasswordInput) as FormControl;
        passwordControl.valueChanges.subscribe(() => {
          confirmPasswordControl.updateValueAndValidity();
        });
      }
  
      if (passwordControl.value !== confirmPasswordControl.value) {
        return { notMatch: true };
      }
  
      return null;
    };
  }

  EmailValidator(confirmEmailInput: string) {
    let confirmEmailControl: FormControl;
    let emailControl: FormControl;
    
    return (control: FormControl) => {
      if (!control.parent) {
        return null;
      }
    
      if (!confirmEmailControl) {
        confirmEmailControl = control;
        emailControl = control.parent.get(confirmEmailInput) as FormControl;
        emailControl.valueChanges.subscribe(() => {
          confirmEmailControl.updateValueAndValidity();
        });
      }
  
      if (emailControl.value.toLocaleLowerCase() !==
          confirmEmailControl.value.toLocaleLowerCase()
      ) {
        return { notMatch: true };
      }
  
      return null;
    };
  }

  get errorControl() {
    //getting para recibir la informacion del formulario
    return this.formGroup.controls;
  }

  get CheckControl() {
    const validar = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
     const value1 = validar.test(this.formGroup.controls['email'].value)
     const value2 = validar.test(this.formGroup.controls['confirm_email'].value)
      if(value1 === true && value2 === true){
        console.log("entrar en la condicion")
        //getting para recibir la informacion del formulario
          if(this.formGroup.controls['email'].value === this.formGroup.controls['confirm_email'].value &&
          this.formGroup.controls['email'].value.length > 5 && this.formGroup.controls['confirm_email'].value.length > 5  ){
          return false
          } else{
          return true
        }
      }else{
        return true
      }
  }

  goTo(url) {
    this.navCtrl.navigateForward(url);
  }

  getData(){
    // this.utilities.displayLoading()

    this.coinkservice.getDocument().subscribe(resp=>{
      this.document = resp
    })

    this.coinkservice.getGender().subscribe(resp=>{
      this.gender = resp
      this.utilities.dismissLoading()
    })

  }

}
