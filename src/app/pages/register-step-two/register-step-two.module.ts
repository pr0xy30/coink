import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterStepTwoPageRoutingModule } from './register-step-two-routing.module';

import { RegisterStepTwoPage } from './register-step-two.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RegisterStepTwoPageRoutingModule
  ],
  declarations: [RegisterStepTwoPage]
})
export class RegisterStepTwoPageModule {}
