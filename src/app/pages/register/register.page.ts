import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UtilitiesService } from "src/app/services/utilities/utilities.service";
import { NavController } from "@ionic/angular";
import { CONSTANTES } from "src/app/services/constantes";
import { EncryptserviceService } from "src/app/services/cifrado/encryptservice.service";
import { environment } from "src/environments/environment";
import { CoinkService } from "src/app/services/coink/coink.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"],
})
export class RegisterPage implements OnInit {
  formGroup: FormGroup;
  miVariable:any = "A";
  collector:any = [];
  showbutton:boolean = false;
  showtemplate:boolean = false;
  phone:any;


  constructor(
    private cifrado: EncryptserviceService,
    private coinkservice: CoinkService,
    private fb: FormBuilder,
    private utilities: UtilitiesService,
    private navCtrl: NavController,
  ) {
    this.formGroup = this.fb.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$"),
        ]),
      ],
      password: ["", Validators.compose([Validators.required])],
      numberpad: ["", Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.formGroup.reset();
  }


  goTo(url) {
    this.navCtrl.navigateForward(url);
  }


  calculator(e){
    if(this.showtemplate){
      this.collector.push(e)
      if(this.collector.length == 4){
         /*Valida Numero del codigo Correcto*/
         /*Pasa ala siguiente pantalla*/
        this.goTo('/register-step-two')
      }
      console.log(this.collector)
      let join = this.collector.join(' ')
      this.formGroup.controls['numberpad'].setValue(join)
    }else{
      if(this.collector.length > 10){
        return;
      }
      this.collector.push(e)
      console.log(this.collector)
      if(this.collector.length == 11){
        this.showbutton = true;
      }
      if(this.collector.length == 3){
        this.collector.splice( 3, 0, " - ")
      }
      let join = this.collector.join(' ')
      this.formGroup.controls['numberpad'].setValue(join)
    }




  }

  remover(){
    this.showbutton = false;
    this.collector.pop()
    let join = this.collector.join(' ')
    this.formGroup.controls['numberpad'].setValue(join)
  }

  approve(){
     

    if(this.showbutton){
      this.utilities.displayLoading()

      let charRemover = this.formGroup.controls['numberpad'].value 
          charRemover = charRemover.replaceAll('-', '')
      this.phone = charRemover
      this.phone = this.phone.replace(/\s/g, "")
      const data = {
          "phone_number": "57"+this.phone,
          "log_signup_id": ""
      }

    /* CIFRADO */
     const cifrado = this.encrypt(data)
      console.log(cifrado)

     this.coinkservice.getSMS(cifrado).subscribe((resp:any)=>{
        const payload = resp.payload
        console.log( payload)
        /* DECIFRAR */
        const decifrado = this.decrypt(payload)
        localStorage.setItem("token", JSON.stringify(decifrado));
        console.log(decifrado)
        this.showtemplate = true;
        this.formGroup.reset();
        this.collector =[]

        this.utilities.dismissLoading()
     },err=>this.message())


    }
  }

  encrypt(payload){
    return this.cifrado.encrypt( JSON.stringify(payload) , environment.key)
  }

  decrypt(payload){
    return this.cifrado.decrypt(payload,environment.key)
  }

  message(){
    this.utilities.dismissLoading()
    this.utilities.errorAlert('NÚMERO INCORRECTO', 'El número que ingresaste es incorrecto, ingrese un número válido.')
  }


}
