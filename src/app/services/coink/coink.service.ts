import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {EMPTY, Observable } from 'rxjs';
import {catchError,retry} from 'rxjs/operators'
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CoinkService {

  authorization: string;
  httpOptions: any;

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' +  environment.key,
        "Access-Control-Allow-Origin": '*',
        'Content-Type':'application/json',
      })
    };
  }

  getSMS(cifrado): Observable<any> {
      const data = {
        payload : cifrado
      } 
    return this.http.post(`${environment.get_sms}`, data, this.httpOptions)
  }

  getDocument(): Observable<any> {
    return this.http.get(`${environment.get_document}`).pipe(
      retry(3),
      catchError(() => {
        return EMPTY
      })
    )
  }

  getGender(): Observable<any> {
    return this.http.get(`${environment.get_gender}`).pipe(
      retry(3),
      catchError(() => {
        return EMPTY
      })
    )
  }

  
}
