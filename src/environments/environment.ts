// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

//eliminar Cors / temporal
const cors = 'https://api-cors-proxy-devdactic.herokuapp.com/'
//URL QA
const urlEnviroment = `${cors}https://api.bancoink.biz/qa/`
//Parametro
const id = "030106"

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDkzMrry8UvFhAbocZgAZeovVGHB5c4tvY",
    authDomain: "pokedex-311901.firebaseapp.com",
    projectId: "pokedex-311901",
    storageBucket: "pokedex-311901.appspot.com",
    messagingSenderId: "1020791984632",
    appId: "1:1020791984632:web:d654d6c876f64d11f6860d",
    measurementId: "G-9MQNP43M3X"
  },
  get_sms: `${urlEnviroment}signup/sendSmsVerificationNumber?apiKey=${id}`,
  get_document: `${urlEnviroment}signup/documentTypes?apiKey=${id}`,
  get_gender: `${urlEnviroment}signup/genders?apiKey=${id}`,
  key:"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJBcGlLZXkiOiIwMzAxMDYiLCJWZXJzaW9uIjoiMS4wLjAifQ.OJ7pEEf3b0tPHwdWIn7-v18tYnMeYhTU9UT8zDSEtrg"
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
